import machine
import micropython

from ponguy import config
from ponguy.constants import RAINBOW_COLORS

START_HEADER_SIZE = 4
LED_START = 0b11100000  # Three "1" bits, followed by 5 brightness bits


class Strip:
    animation = None
    previous_animation = None
    reversed_display = False

    def __init__(self):
        self.num_leds = config.NUM_LEDS - config.END_OFFSET

        self._spi = machine.SPI(
            2,
            baudrate=10000000,
            sck=machine.Pin(config.LED_CLOCK_PIN),
            mosi=machine.Pin(config.LED_PIN),
            miso=machine.Pin(19),
        )  # miso pin is not used but required

        brightness = max(min(config.BRIGHTNESS, 30), 1)
        self.brightness_byte = brightness | LED_START

        self.allocate_buffer()

        self.num_leds -= config.START_OFFSET

        self.rainbow_buffer = self.get_color_buffer(RAINBOW_COLORS)
        self.rainbow_colors_count = len(RAINBOW_COLORS)

    def allocate_buffer(self):
        # taken from https://github.com/mattytrentini/micropython-dotstar

        # Supply one extra clock cycle for each two pixels in the strip.
        self.end_header_size = self.num_leds // 16
        if self.num_leds % 16 != 0:
            self.end_header_size += 1
        self._buf = bytearray(
            self.num_leds * 4 + START_HEADER_SIZE + self.end_header_size
        )
        self.end_header_index = len(self._buf) - self.end_header_size

        # Four empty bytes to start.
        for i in range(START_HEADER_SIZE):
            self._buf[i] = 0x00

        # Set brightness of each pixel.
        for i in range(START_HEADER_SIZE, self.end_header_index, 4):
            self._buf[i] = self.brightness_byte

        # null bytes at the end.
        for i in range(self.end_header_index, len(self._buf)):
            self._buf[i] = 0x00

        offset = config.START_OFFSET * 4 + START_HEADER_SIZE
        self.buf = memoryview(self._buf)[offset : self.end_header_index]
        self.buf_size_32 = len(self.buf) // 4

        self._black_buffer = self._buf[offset : self.end_header_index]

    @micropython.viper
    def _set_led(self, index: int, red: int, green: int, blue: int):
        buf = ptr32(self.buf)
        buf[index] = ((red << 8 | green) << 8 | blue) << 8 | int(self.brightness_byte)

    def show(self):
        if self.reversed_display:
            self.reverse_buffer()
        self._spi.write(self._buf)

    @micropython.native
    def turn_on(self, color, position):
        self._set_led(position, *color)

    @micropython.native
    def turn_off(self, position):
        self._set_led(position, 0, 0, 0)

    @micropython.native
    def turn_range_on(self, color, start, end):
        self.turn_range_on_viper(color[0], color[1], color[2], start, end)

    @micropython.viper
    def turn_range_on_viper(
        self, red: int, green: int, blue: int, start: int, end: int
    ):
        buf = ptr32(self.buf)
        color = ((red << 8 | green) << 8 | blue) << 8 | int(self.brightness_byte)

        for i in range(start, end + 1):
            buf[i] = color

    @micropython.viper
    def get_buffer(self, size: int):
        return bytearray(size * 4)

    @micropython.viper
    def get_color_buffer(self, colors):
        colors_len = int(len(colors))
        buffer = self.get_buffer(colors_len)
        self.fill_color_buffer(colors, buffer, colors_len)
        return buffer

    @micropython.viper
    def fill_color_buffer(self, colors, buffer, buffer_size):
        buffer_ptr = ptr32(buffer)
        brightness_byte = int(self.brightness_byte)

        for i in range(int(buffer_size)):
            r, g, b = colors[i]
            buffer_ptr[i] = (
                (int(r) << 8 | int(g)) << 8 | int(b)
            ) << 8 | brightness_byte

    @micropython.viper
    def turn_range_on_from_buffer(
        self, colors: ptr32, start: int, end: int, start_offset: int
    ):
        buf = ptr32(self.buf)
        colors = ptr32(colors)  # needed for unit tests

        for i in range(start, end + 1):
            buf[i] = colors[i - start + start_offset]

    @micropython.viper
    def turn_range_on_rainbow(
        self, start: int, end: int, rainbow_start: int, step: int
    ):
        buf = ptr32(self.buf)
        colors = ptr32(self.rainbow_buffer)
        colors_count = int(self.rainbow_colors_count)

        for i in range(start, end + 1):
            buf[i] = colors[(rainbow_start + i * step) % colors_count]

    @micropython.viper
    def fade_range_by(self, start: int, end: int, amount: int):
        buf = ptr8(self.buf)

        for i in range(start, end + 1):
            index = i * 4
            buf[index + 1] = (buf[index + 1] * amount) >> 8
            buf[index + 2] = (buf[index + 2] * amount) >> 8
            buf[index + 3] = (buf[index + 3] * amount) >> 8

    @micropython.native
    def turn_all_off(self):
        self.buf[:] = self._black_buffer

    @micropython.viper
    def reverse_buffer(self):
        buf = ptr32(self.buf)
        buf_size = int(self.buf_size_32)
        for i in range(buf_size // 2):
            buf[i], buf[buf_size - 1 - i] = buf[buf_size - 1 - i], buf[i]

    def set_animation(self, animation):
        self.animation = animation
        self.animation.init_anim()

    def animate(self):
        if self.animation:
            if self.animation.done():
                self.previous_animation = self.animation
                self.animation = None
            else:
                self.animation.play()
        return bool(self.animation is not None)
