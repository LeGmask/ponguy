from random import randint

import machine
import micropython

from ponguy.constants import RAINBOW_COLORS


def get_random_color():
    color_index = randint(0, len(RAINBOW_COLORS) - 1)
    return RAINBOW_COLORS[color_index]


@micropython.native
def fade_to_black(color, amount):
    return tuple((x * int(amount)) >> 8 for x in color)


class FakePin(machine.Pin):
    def __init__(self):
        super().__init__(1)

    def value(self, *args, **kwargs):
        return 1
