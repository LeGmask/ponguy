import machine

from ponguy import Colors, config
from ponguy.game import Game
from ponguy.player import Player
from ponguy.screensaver import Screensaver
from ponguy.strip import Strip

strip = Strip()
screensaver = Screensaver(strip)
players = [
    Player(
        machine.Pin(config.RED_BUTTON_PIN, machine.Pin.IN, machine.Pin.PULL_UP),
        Colors.RED,
        0,
        1,
    ),
    Player(
        machine.Pin(config.BLUE_BUTTON_PIN, machine.Pin.IN, machine.Pin.PULL_UP),
        Colors.BLUE,
        strip.num_leds - 1,
        -1,
    ),
]
game = Game(strip, players)


for pin in config.PINS_TO_POWER:
    pin = machine.Pin(pin, machine.Pin.OUT)
    pin.on()


def run():
    strip.show()

    if game.state != game.WAITING:
        game.play()
    elif game.should_start():
        game.init_game()
    else:
        screensaver.animate()
